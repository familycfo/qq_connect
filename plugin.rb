# name: QQ connect
# about: Authenticate with discourse with qq connect.
# version: 0.4.0
# author: Erick Guan
# url: https://github.com/fantasticfears/qq_connect

# gem 'omniauth-qq-connect', '0.2.0'
# gem "ruby-pinyin", "0.4.5"

require 'omniauth/strategies/oauth2'

if respond_to?(:register_svg_icon)
  register_svg_icon "fab-qq"
end

register_asset 'stylesheets/qq-login.scss'

enabled_site_setting :QQ_enabled


class OmniAuth::Strategies::QQConnect < OmniAuth::Strategies::OAuth2
  option :name, "qq_connect"

  option :client_options, {
                          :site => 'https://graph.qq.com/oauth2.0/',
                          :authorize_url => '/oauth2.0/authorize',
                          :token_url => "/oauth2.0/token"
                        }

  option :token_params, {
                        :state => 'foobar',
                        :parse => :query
                      }

  uid do
    @uid ||= begin
      access_token.options[:mode] = :query
      access_token.options[:param_name] = :access_token
      # Response Example: "callback( {\"client_id\":\"11111\",\"openid\":\"000000FFFF\"} );\n"
      response = access_token.get('/oauth2.0/me')
      #TODO handle error case
      matched = response.body.match(/"openid":"(?<openid>\w+)"/)
      matched[:openid]
    end
  end

  info do
    {
      :nickname => raw_info['nickname'],
      :name => raw_info['nickname'],
      :image => raw_info['figureurl_qq_1'],
    }
  end

  extra do
    {
      :raw_info => raw_info
    }
  end

  def raw_info
    @raw_info ||= begin
                    #TODO handle error case
                    #TODO make info request url configurable
      client.request(:get, "https://graph.qq.com/user/get_user_info", :params => {
                           :format => :json,
                           :openid => uid,
                           :oauth_consumer_key => options[:client_id],
                           :access_token => access_token.token
                         }, :parse => :json).parsed
    end
  end
end

OmniAuth.config.add_camelization('qq_connect', 'QQConnect')

# Discourse plugin
class Auth::QQAuthenticator < ::Auth::ManagedAuthenticator

  def name
    'qq_connect'
  end

  def enabled?
    SiteSetting.QQ_enabled
  end

  def match_by_email
    false
  end

  def can_revoke?
    true
  end

  def can_connect_existing_user?
    true
  end

  def register_middleware(omniauth)
    omniauth.provider :qq_connect, :setup => lambda { |env|
      strategy = env['omniauth.strategy']
      strategy.options[:client_id] = SiteSetting.qq_connect_client_id
      strategy.options[:client_secret] = SiteSetting.qq_connect_client_secret
    }
  end
end

auth_provider  frame_width: 760,
               frame_height: 500,
               authenticator: Auth::QQAuthenticator.new,
               icon: 'fab-qq'
